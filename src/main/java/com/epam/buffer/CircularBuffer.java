package com.epam.buffer;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class CircularBuffer<T> implements Buffer<T> {

    private final T[] buffer;
    private final int capacity;
    private int head;
    private int tail;

    public CircularBuffer(final Class<T> type, final int capacity) {
        this.capacity = capacity;
        buffer = (T[]) Array.newInstance(type, capacity);
        head = 0;
        tail = 0;

    }

    @Override
    public void put(T t) {
        if (head == tail && !isEmpty()) {
            throw new RuntimeException("Buffer is full!");
        }
        buffer[head] = t;
        head++;
        if (head == capacity) {
            head = 0;
        }
    }

    @Override
    public T get() {
        if (head == tail && isEmpty()) {
            throw new RuntimeException("Buffer is empty!");
        }
        T value = buffer[tail];
        buffer[tail] = null;
        tail++;
        if (tail == capacity) {
            tail = 0;
        }
        return value;
    }

    @Override
    public Object[] toObjectArray() {
        return buffer.clone();
    }

    @Override
    public T[] toArray() {
        return (T[]) Arrays.copyOf(buffer, capacity, buffer.getClass());
    }

    @Override
    public List<T> asList() {
        return new ArrayList<>(Arrays.asList(buffer));
    }

    @Override
    public void addAll(List<? extends T> toAdd) {
        toAdd.forEach(this::put);
    }

    @Override
    public void sort(Comparator<? super T> comparator) {
        Arrays.sort(buffer, comparator);
    }

    @Override
    public boolean isEmpty() {
        boolean empty = true;
        for (int i = 0; i < capacity; i++) {
            empty &= buffer[i] == null;
        }
        return empty;
    }

    @Override
    public String toString() {
        return "CircularBuffer{" +
                "buffer=" + Arrays.toString(buffer) +
                ", capacity=" + capacity +
                ", head=" + head +
                ", tail=" + tail +
                '}';
    }
}
