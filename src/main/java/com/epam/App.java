package com.epam;

import com.epam.buffer.Buffer;
import com.epam.buffer.CircularBuffer;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class App {
    private final Buffer<Integer> buffer;

    public App() {
        buffer = new CircularBuffer<>(Integer.class, 5);
    }

    public static void main(String[] args) {
        new App().run();
    }

    private void run() {
        buffer.put(5);
        buffer.put(3);
        buffer.put(7);
        System.out.println(buffer);
        System.out.println(buffer.get());
        System.out.println(buffer);
        buffer.put(9);
        buffer.put(2);
        System.out.println(buffer);
        buffer.put(1);
        System.out.println(buffer);
        Integer[] integers = buffer.toArray();
        System.out.println( Arrays.toString(integers));
        Object[] objects = buffer.toObjectArray();
        System.out.println( Arrays.toString(objects));
        List<Integer> integersList = buffer.asList();
        System.out.println(integersList);
        buffer.sort(Comparator.naturalOrder());
        System.out.println(buffer);
        buffer.put(11);
    }

}
